#!/bin/bash
set -x
cd ~/Projects/crawl

d="adstxt_$(date +%Y-%m-%d).db"
sqlite3 "${d}" < adstxt_crawler.sql
rm adstxt_crawler.log


python2.7 ./adstxt_crawler.py -t all_dinco_top_au_domains.txt -d "adstxt_$(date +%Y-%m-%d).db"
sqlite3 "adstxt_$(date +%Y-%m-%d).db" <<!
.headers on
.mode csv
.output "adstxt_$(date +%Y-%m-%d).csv"
select * from adstxt;
!
