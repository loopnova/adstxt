#!/bin/sh

set -x

d="adstxt_$(date +%Y-%m-%d).db"
sqlite3 "${d}" < adstxt_crawler.sql
rm adstxt_crawler.log
